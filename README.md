# YouTube Vanced+ [PlaceHolder]

![GitLab Forks](https://img.shields.io/gitlab/forks/46558790?gitlab_url=https%3A%2F%2Fgitlab.com&style=for-the-badge&logo=gitlab)
![GitLab Stars](https://img.shields.io/gitlab/stars/46558790?gitlab_url=https%3A%2F%2Fgitlab.com&style=for-the-badge&logo=gitlab)

This is **PLACEHOLDER** project and currently not download or buildable. No ETAs yet. YOU can try other project like [NewPipe](https://github.com/TeamNewPipe/NewPipe), [LibreTube](https://github.com/libre-tube/LibreTube) or [SmartTube for Android TV](https://github.com/yuliskov/SmartTube).

**Note** : For now, Vanced+ are **still not published** yet. I know that is **bad things**, no one actually **trust this project exists** at all but that **everything i can say right now**. i'll notify you when **Vanced+ ready on these places** ! 


 <a href="https://t.me/ytappver" ><img src="https://img.shields.io/badge/YouTube Vanced+ Channel-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white"></a>
</p>
 <a href="https://youtube.com/@cuynu/community" ><img src="https://img.shields.io/badge/YouTube-red?style=for-the-badge&logo=youtube&label=Channel%20Community&logoColor=white"></a>
<p align="left">
  <a href="https://discord.gg/WF9E4fGq8Z">
    <img alt="Discord" src="https://img.shields.io/discord/1077997663628296333?color=%2300C853&label=YouTube%20VancedX%20Server&logo=discord&logoColor=%2300C853&style=for-the-badge">
  </a>

![Subreddit subscribers](https://img.shields.io/reddit/subreddit-subscribers/cuynuvanced?style=for-the-badge&logo=reddit&label=Reddit%20r%2Fcuynuvanced&link=https%3A%2F%2Fwww.reddit.com%2Fr%2Fcuynuvanced%2F)



-------------------------

## Attention 
- By using Vanced+ or share anything related to Vanced+ to YouTube, you are violating YouTube Terms of Service : [About Software in the Service](https://www.youtube.com/t/terms#8c38269fae) !
- Using at your own risk !
- Do not upload anything related to Vanced+ to YouTube. Otherwise, your channel might be get banned due to violating YouTube Term of Service mentioned above !


## Source code
- Project source code are stored on seperate repository, not this vancedx repository.
- As project are still placeholder and nothing been started, so for now, i'll kept everything private except this repo and VancedxMicroG until i change my life and continue with implement Vanced+

## Frequently Asked Questions

[See FAQs on Wiki](https://gitlab.com/cuynu/vancedx/-/wikis/Frequently-Asked-Questions).


## Evolution of repo 
- This repo was originally created for archive the discontinued version of Vanced (17.03.38) on 2022/03 on GitHub, See commit : [e14f2a59](https://gitlab.com/cuynu/vancedx/-/tree/e14f2a598e62192543d699d2109d68ed4d4e26aa)
- Around 2022/07, this repo are used for providing unofficial Vanced which is released by inotia00 on GitHub, See commit : [90fe1ec2](https://gitlab.com/cuynu/vancedx/-/tree/90fe1ec26aa16613b9d97f973da379080b69b7c8)
- Around 2022/10 till 2023/06, this repo switched to newer unofficial Vanced which is BASED on ReVanced stuffs to works (literally just ReVanced but rebranded to Vanced logo), See first commit : [1d5923f1](https://gitlab.com/cuynu/vancedx/-/tree/1d5923f155d18d7435b5662c8d66d681c74b04c0) and last commit : [cb88d8ef](https://gitlab.com/cuynu/vancedx/-/tree/cb88d8efa70cac397ac091ad242cc3a8af8e6396)
- Around 2023/06 till right now, the old project that this repo handle are partially removed and replaced with "Vanced+", but Vanced+ are still Placeholder : First commit : [3cff5d39](https://gitlab.com/cuynu/vancedx/-/tree/3cff5d39527d9d0dbddd8a66e71782e13b79e781)
- This repo are completely switched to GitLab on 2023/31/12, but on GitHub, there still repository that redirect users to this GitLab repo.


## Credits

- The icon of Aria inside Vanced+ icon was created by [@sevenc_nanashi@voskey.icalo.net](https://voskey.icalo.net/@sevenc_nanashi) and is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).

- **[Team Vanced](https://github.com/TeamVanced)** : Old YouTube Vanced official which is closed source, I use its icon assets (Vanced, SponsorBlock, RYD icons)

- **[inotia00](https://github.com/inotia00)** : Old YouTube Vanced (RVX) based patches which is used on Unofficial Vanced v17.34.36-v18.21.34 (this is my bad past)

- **[ReVanced Team](https://github.com/revanced)** : ReVanced Team for CLI used to build old Unofficial Vanced

-------------------------
