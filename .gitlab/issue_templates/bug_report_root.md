

PLEASE FILL OUT THIS FORM, DO NOT CREATE ISSUE WITHOUT THIS FORM, OTHERWISE, ISSUE WILL CLOSE WITHOUT ANY NOTICE !!!

Make sure that bug only exists in Vanced+ app and NOT in original YouTube app with the same version of Vanced+ !!!

Environment
===========
Platform: Android

Android Version:

YouTube Vanced+ Version:

Google Play Services (GMS) version : 
 
Device name: 

Module Download Link (leave empty if manually builded) : 

Description
===========


Steps to reproduce
==================
1.
2.
3.
4.


Expected result
===============


Actual result
=============


Screenshots / Videos / Logs
===========================


Notes
=====


